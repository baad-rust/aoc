use std::str::Lines;

#[derive(Debug)]
struct RoundData {
    it: Lines<'static>,
}

#[derive(Debug)]
struct RoundData2 {
    it: Lines<'static>,
}

impl RoundData {
    fn new(data: &'static str) -> Self {
        Self { it: data.lines() }
    }
}

impl RoundData2 {
    fn new(data: &'static str) -> Self {
        Self { it: data.lines() }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Play {
    Rock,
    Paper,
    Scissors,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum MatchResult {
    Lose,
    Draw,
    Win,
}

fn translate_play(input: &str) -> Play {
    match input {
        "A" | "X" => Play::Rock,
        "B" | "Y" => Play::Paper,
        "C" | "Z" => Play::Scissors,
        _ => panic!("Invalid data"),
    }
}

fn get_result(player: Play, opponent: Play) -> MatchResult {
    match (player, opponent) {
        (Play::Rock, Play::Paper)
        | (Play::Paper, Play::Scissors)
        | (Play::Scissors, Play::Rock) => MatchResult::Lose,
        (Play::Paper, Play::Rock)
        | (Play::Scissors, Play::Paper)
        | (Play::Rock, Play::Scissors) => MatchResult::Win,
        _ => MatchResult::Draw,
    }
}

fn get_play_score(play: Play) -> u32 {
    match play {
        Play::Rock => 1,
        Play::Paper => 2,
        Play::Scissors => 3,
    }
}

fn get_match_score(result: MatchResult) -> u32 {
    match result {
        MatchResult::Lose => 0,
        MatchResult::Draw => 3,
        MatchResult::Win => 6,
    }
}

fn need_win(play: Play) -> Play {
    match play {
        Play::Rock => Play::Paper,
        Play::Paper => Play::Scissors,
        Play::Scissors => Play::Rock,
    }
}

fn need_lose(play: Play) -> Play {
    match play {
        Play::Rock => Play::Scissors,
        Play::Paper => Play::Rock,
        Play::Scissors => Play::Paper,
    }
}

fn translate_requirement(input: &str, play: Play) -> (Play, MatchResult) {
    match input {
        "X" => (need_lose(play), MatchResult::Lose),
        "Y" => (play, MatchResult::Draw),
        "Z" => (need_win(play), MatchResult::Win),
        _ => panic!("Invalid data"),
    }
}

impl Iterator for RoundData {
    type Item = (Play, Play, MatchResult);

    fn next(&mut self) -> Option<Self::Item> {
        let mut round = self.it.next()?.split(' ');
        let opponent = translate_play(round.next()?);
        let player = translate_play(round.next()?);
        Some((player, opponent, get_result(player, opponent)))
    }
}

impl Iterator for RoundData2 {
    type Item = (Play, Play, MatchResult);

    fn next(&mut self) -> Option<Self::Item> {
        let mut round = self.it.next()?.split(' ');
        let opponent = translate_play(round.next()?);
        let player = translate_requirement(round.next()?, opponent);
        Some((player.0, opponent, player.1))
    }
}

fn main() {
    let data = include_str!("data.txt");

    let rounds = RoundData::new(data);
    let score: u32 = rounds
        .map(|g| get_play_score(g.0) + get_match_score(g.2))
        .sum();

    println!("1st Score = {score}");

    let rounds = RoundData2::new(data);
    let score: u32 = rounds
        .map(|g| get_play_score(g.0) + get_match_score(g.2))
        .sum();

    println!("2nd Score = {score}");
}
