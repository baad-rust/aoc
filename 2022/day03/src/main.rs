#![allow(unused, dead_code)]

use std::{collections::HashSet, str::Lines};

use itertools::Itertools;

struct Rucksacks {
    it: Lines<'static>,
}

impl Rucksacks {
    fn new(data: &'static str) -> Self {
        Self { it: data.lines() }
    }
}

impl Iterator for Rucksacks {
    type Item = Rucksack;

    fn next(&mut self) -> Option<Self::Item> {
        match self.it.next() {
            Some(line) => Some(Rucksack::new(line)),
            None => None,
        }
    }
}

#[derive(Debug)]
struct Rucksack {
    compartment: [String; 2],
}

impl Rucksack {
    fn new(items: &str) -> Self {
        let num_items = items.len();
        assert_eq!(num_items % 2, 0);

        let (comp1, comp2) = items.split_at(num_items / 2);

        Self {
            compartment: [comp1.to_string(), comp2.to_string()],
        }
    }

    fn find_common_items(&self) -> String {
        items_set(&self.compartment[0])
            .intersection(&items_set(&self.compartment[1]))
            .collect()
    }

    fn items_set(&self) -> HashSet<char> {
        &items_set(&self.compartment[0]) | &items_set(&self.compartment[1])
    }
}

fn items_set(items: &str) -> HashSet<char> {
    items.chars().collect()
}

fn get_priority(item: char) -> u32 {
    if item >= 'a' && item <= 'z' {
        item as u32 - ('a' as u32) + 1
    } else if item >= 'A' && item <= 'Z' {
        item as u32 - ('A' as u32) + 27
    } else {
        panic!("Invalid item type: {item}");
    }
}

fn main() {
    let data = include_str!("data.txt");

    let sacks = Rucksacks::new(data);
    puzzle1(sacks);

    let sacks = Rucksacks::new(data);
    puzzle2(sacks);
}

fn puzzle1(sacks: Rucksacks) {
    let sum_priorities: u32 = sacks
        .map(|sack| get_priority(sack.find_common_items().chars().next().unwrap()))
        .sum();

    dbg!(sum_priorities);
}

fn puzzle2(sacks: Rucksacks) {
    let sack_groups = sacks
        .chunks(3)
        .into_iter()
        .map(|chunk| chunk.map(|sack| sack.items_set()).collect::<Vec<_>>())
        // sack_groups now contains a vector or groups, each a vector of hashsets.
        // we now fold the vector of sets to see the common items
        .map(|group| {
            group
                .iter()
                .fold(group[0].clone(), |acc, set| &acc & &set)
                .iter()
                .next()
                .unwrap()
                .clone()
        })
        .map(get_priority)
        .sum::<u32>();

    dbg!(sack_groups);
}
