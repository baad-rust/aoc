// #![allow(unused, dead_code)]

use std::str::Lines;

use itertools::sorted;

fn main() {
    let data = include_str!("../data/calories.txt");

    let calories = Calories::new(data);
    dbg!(calories.max());

    let calories = Calories::new(data);
    dbg!(sorted(calories).rev().take(3).sum::<i32>());
}

#[derive(Debug, Clone, Copy)]
enum CaloriesEntry {
    Calories(i32),
    None,
}

#[derive(Debug)]
struct CaloriesData {
    it: Lines<'static>,
}

impl CaloriesData {
    fn new(data: &'static str) -> Self {
        let it = data.lines();
        Self { it }
    }
}

impl Iterator for CaloriesData {
    type Item = CaloriesEntry;

    fn next(&mut self) -> Option<Self::Item> {
        match self.it.next() {
            Some("") => Some(CaloriesEntry::None),
            Some(line) => Some(CaloriesEntry::Calories(line.parse().unwrap())),
            None => None,
        }
    }
}

#[derive(Debug)]
struct Calories {
    it: CaloriesData,
}

impl Calories {
    fn new(data: &'static str) -> Self {
        Self {
            it: CaloriesData::new(data),
        }
    }
}

impl Iterator for Calories {
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        let mut count = 0;
        loop {
            match self.it.next() {
                Some(CaloriesEntry::None) => break Some(count),
                Some(CaloriesEntry::Calories(value)) => count += value,
                None => break None,
            }
        }
    }
}
