use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{self, alpha1, char, newline},
    combinator::map,
    multi::{many0, separated_list1},
    sequence::delimited,
    IResult,
};

fn main() -> color_eyre::Result<()> {
    color_eyre::install()?;

    let data = include_str!("data.txt");
    let mut crates = Puzzle::new(data);

    // dbg!(crates);

    dbg!(crates.process_moves());

    Ok(())
}

/// A representation of a single crate.
#[derive(Debug, Clone, Copy)]
struct Crate(char);

/// Encoding of each move line.
#[derive(Debug)]
struct Move {
    num: usize,
    from: usize,
    to: usize,
}

/// The crate stacks are stored here
#[derive(Debug)]
struct Puzzle {
    stacks: Vec<Vec<Crate>>,
    moves: Vec<Move>,
}

impl Puzzle {
    fn new(data: &str) -> Self {
        let mut sections = data.split("\n\n");
        let arrangements = sections.next().unwrap();
        let moves = sections.next().unwrap();

        // Process blocks
        let mut next_tokens = parse_stacks(arrangements).unwrap().1;
        next_tokens.pop();

        let width = next_tokens.iter().map(|line| line.len()).max().unwrap();
        let mut stacks: Vec<Vec<Crate>> = Vec::new();
        stacks.resize_with(width, Vec::new);

        for line in next_tokens {
            for (column, block) in line.iter().enumerate() {
                match block {
                    Block::Gap => {}
                    Block::Crate(c) => stacks[column].push(Crate(*c)),
                }
            }
        }

        // We need to reverse the stacks are they are backwards for push/pop to work.
        let stacks = stacks
            .into_iter()
            .map(|stack| stack.iter().rev().copied().collect::<Vec<Crate>>())
            .collect::<Vec<_>>();

        // Parse the moves
        let moves = moves
            .lines()
            .map(|line| parse_moves(line).unwrap().1)
            .collect();

        Self { stacks, moves }
    }

    fn process_moves(&mut self) -> String {
        for m in &self.moves {
            let mut t = Vec::new();
            for _ in 0..m.num {
                let c = self.stacks[m.from].pop().unwrap();
                t.push(c);
            }
            for _ in 0..m.num {
                let c = t.pop().unwrap();
                self.stacks[m.to].push(c);
            }
        }

        self.stacks
            .iter()
            .map(|stack| stack.last().unwrap().0)
            .collect::<String>()
    }
}

#[derive(Debug)]
enum Block {
    Gap,
    Crate(char),
}

fn parse_block(input: &str) -> IResult<&str, Block> {
    let block_empty = map(tag("    "), |_| Block::Gap);
    let block_crate = map(delimited(char('['), alpha1, tag("] ")), |c: &str| {
        Block::Crate(c.chars().next().unwrap())
    });
    let (input, result) = alt((block_empty, block_crate))(input)?;

    Ok((input, result))
}

fn parse_blocks(input: &str) -> IResult<&str, Vec<Block>> {
    let (input, blocks) = many0(parse_block)(input)?;

    Ok((input, blocks))
}

fn parse_stacks(input: &str) -> IResult<&str, Vec<Vec<Block>>> {
    let (input, stacks) = separated_list1(newline, parse_blocks)(input)?;

    Ok((input, stacks))
}

fn parse_moves(input: &str) -> IResult<&str, Move> {
    let (input, _) = tag("move ")(input)?;
    let (input, num) = map(complete::u64, |num| num as usize)(input)?;
    let (input, _) = tag(" from ")(input)?;
    let (input, from) = map(complete::u64, |from| (from - 1) as usize)(input)?;
    let (input, _) = tag(" to ")(input)?;
    let (input, to) = map(complete::u64, |to| (to - 1) as usize)(input)?;

    Ok((input, Move { num, from, to }))
}
