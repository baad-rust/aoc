//#![allow(unused)]

use std::str::{FromStr, Lines};

use nom::{
    character::complete::char,
    character::complete::digit1,
    combinator::{map, map_res},
    sequence::separated_pair,
    IResult,
};

#[derive(Debug)]
struct Range {
    start: u32,
    end: u32,
}

fn parse_number(input: &str) -> IResult<&str, u32> {
    map_res(digit1, u32::from_str)(input)
}

impl Range {
    fn parse(input: &str) -> IResult<&str, Self> {
        let parse_range = separated_pair(&parse_number, char('-'), &parse_number);

        map(parse_range, |(start, end)| Self { start, end })(input)
    }
}

#[derive(Debug)]
struct Assignment {
    elf1: Range,
    elf2: Range,
}

impl Assignment {
    fn parse(input: &str) -> IResult<&str, Self> {
        let parse_assignment = separated_pair(Range::parse, char(','), Range::parse);
        map(parse_assignment, |(range1, range2)| Self {
            elf1: range1,
            elf2: range2,
        })(input)
    }

    fn does_contain(&self) -> bool {
        (self.elf1.start <= self.elf2.start && self.elf1.end >= self.elf2.end)
            || (self.elf1.start >= self.elf2.start && self.elf1.end <= self.elf2.end)
    }

    fn does_overlap(&self) -> bool {
        //  +-----------+
        //      +-------------+
        //
        //      +-------------+
        //  +-----------+
        (self.elf1.start <= self.elf2.start && self.elf1.end >= self.elf2.start)
            || (self.elf2.start <= self.elf1.start && self.elf2.end >= self.elf1.start)
    }
}

struct Assignments {
    it: Lines<'static>,
}

impl Assignments {
    fn new(data: &'static str) -> Self {
        Self { it: data.lines() }
    }
}

impl Iterator for Assignments {
    type Item = Assignment;

    fn next(&mut self) -> Option<Self::Item> {
        let line = self.it.next()?;

        let (_, assignment) = Assignment::parse(line).ok()?;

        Some(assignment)
    }
}

fn main() {
    let assignments = Assignments::new(include_str!("data.txt"));
    dbg!(assignments
        .filter(|assignment| assignment.does_contain())
        .count());

    let assignments = Assignments::new(include_str!("data.txt"));
    dbg!(assignments
        .filter(|assignment| assignment.does_overlap())
        .count());
}
