use itertools::Itertools;

const START_PACKET_SIZE: usize = 4;
const START_MSG_SIZE: usize = 14;

fn main() {
    let data = include_bytes!("data.txt");
    let start_packet = get_start_pos(data, START_PACKET_SIZE);
    let start_msg = get_start_pos(data, START_MSG_SIZE);

    dbg!(start_packet);
    dbg!(start_msg);
}

fn get_start_pos(data: &[u8], message_size: usize) -> usize {
    data.windows(message_size)
        .enumerate()
        .find(|&(_, chars)| chars.iter().unique().count() == message_size)
        .map(|x| x.0 + message_size)
        .unwrap()
}
