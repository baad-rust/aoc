use std::{
    fs::File,
    io::{BufRead, BufReader},
};

fn main() {
    let file = File::open("data/calibration.txt").expect("File not found");
    let reader = BufReader::new(file);

    let sum = reader
        .lines()
        // Produce an interation of iterators of the digits of each line, ignoring errors
        .filter_map(|line| match line {
            Ok(line) => Some(
                line.chars()
                    .filter_map(|c| c.to_digit(10))
                    .collect::<Vec<_>>(),
            ),
            Err(_) => None,
        })
        .map(|digits| {
            // Combine the first and last digit of each line to create a 2 digit number
            digits[0] * 10 + digits[digits.len() - 1]
        })
        .sum::<u32>();

    // Print the sum of the numbers
    println!("Day 1 (part 1): {}", sum);

    let file = File::open("data/calibration.txt").expect("File not found");
    let reader = BufReader::new(file);
    let sum = reader
        .lines()
        .filter_map(|line| match line {
            Ok(line) => Some(DigitExtractor::new(line.as_bytes()).collect::<Vec<_>>()),
            Err(_) => None,
        })
        .map(|digits| {
            // Combine the first and last digit of each line to create a 2 digit number
            digits[0] * 10 + digits[digits.len() - 1]
        })
        .sum::<u32>();

    // Print the sum of the numbers
    println!("Day 1 (part 2): {}", sum);
}

struct DigitExtractor<'a> {
    text: &'a [u8],
    index: usize,
}

impl<'a> DigitExtractor<'a> {
    fn new(text: &'a [u8]) -> Self {
        Self { text, index: 0 }
    }
}

impl<'a> Iterator for DigitExtractor<'a> {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        const DIGITS: [&'static [u8]; 10] = [
            b"zero", b"one", b"two", b"three", b"four", b"five", b"six", b"seven", b"eight",
            b"nine",
        ];

        while self.index < self.text.len() {
            match to_digit(self.text[self.index]) {
                Some(digit) => {
                    self.index += 1;
                    return Some(digit);
                }
                None => {
                    // It's not a digit, so try to match it to a word
                    for (i, word) in DIGITS.iter().enumerate() {
                        if self.text[self.index..].starts_with(word) {
                            self.index += 1;
                            return Some(i as u32);
                        }
                    }
                    // No match, so skip this character
                    self.index += 1;
                }
            }
        }
        // No more digits found
        None
    }
}

fn to_digit(c: u8) -> Option<u32> {
    match c {
        b'0'..=b'9' => Some((c - b'0') as u32),
        _ => None,
    }
}
