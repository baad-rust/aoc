use std::{collections::HashSet, fs::read_to_string};

// Represents a schematic file
pub struct Schematic {
    // The lines of the schematic file
    lines: Vec<String>,
    max_width: usize,
}

impl Schematic {
    pub fn load(file_name: &str) -> Self {
        let lines = read_to_string(file_name)
            .expect("Failed to read file")
            .lines()
            .map(|line| line.to_string())
            .collect::<Vec<String>>();
        let max_width = lines.iter().map(|line| line.len()).max().unwrap_or(0);
        Self { lines, max_width }
    }

    pub fn iter(&self) -> SchematicPartsIterator {
        SchematicPartsIterator::new(self)
    }

    // Returns the character at (x, y) if it exists
    pub fn get_char(&self, x: i32, y: i32) -> Option<char> {
        if x < 0 || y < 0 || x >= self.max_width as i32 || y >= self.lines.len() as i32 {
            return None;
        }
        let line = &self.lines[y as usize];
        line.chars().nth(x as usize)
    }

    // Returns all the parts surrounding (x, y).  A part is a character that is
    // not a digit or a period.  We check orthogonally and diagonally.
    pub fn get_parts(&self, x: i32, y: i32) -> Vec<(char, i32, i32)> {
        let mut parts = Vec::new();

        for y_offset in -1..=1 {
            for x_offset in -1..=1 {
                if x_offset == 0 && y_offset == 0 {
                    continue;
                }
                let c = self.get_char(x + x_offset, y + y_offset);
                if c.is_none() {
                    continue;
                }
                let c = c.unwrap();
                if !c.is_ascii_digit() && c != '.' {
                    parts.push((c, x + x_offset, y + y_offset));
                }
            }
        }
        parts
    }
}

pub struct SchematicPartsIterator<'a> {
    schematic: &'a Schematic,
    x: i32,
    y: i32,
}

impl<'a> SchematicPartsIterator<'a> {
    pub fn new(schematic: &'a Schematic) -> Self {
        Self {
            schematic,
            x: 0,
            y: 0,
        }
    }

    // Returns the character at (x, y) if it exists
    pub fn get_char(&self, x: i32, y: i32) -> Option<char> {
        self.schematic.get_char(x, y)
    }

    // Returns the maximum width of the schematic
    pub fn max_width(&self) -> i32 {
        self.schematic.max_width as i32
    }

    // Advances coordinates to the next character.  Returns true if we've
    // reached the end of the line.
    pub fn advance(&mut self) -> bool {
        self.x += 1;
        if self.x >= self.max_width() {
            self.x = 0;
            self.y += 1;
        }
        self.x == 0
    }
}

// Returns a de-dupe'd list of parts of type (part, x, y).
pub fn dedup_parts(parts: Vec<(char, i32, i32)>) -> Vec<(char, i32, i32)> {
    parts
        .into_iter()
        .collect::<HashSet<(char, i32, i32)>>()
        .into_iter()
        .collect()
}

impl Iterator for SchematicPartsIterator<'_> {
    // The iterator will return a tuple of (part_number, is_part).
    type Item = (u32, Vec<(char, i32, i32)>);

    fn next(&mut self) -> Option<Self::Item> {
        // Find the next digit in the schematic
        loop {
            // If there is no character we are out of bounds and since (x, y) is
            // always in bounds until we've run out of lines, we are done.
            let mut c = self.get_char(self.x, self.y)?;
            let mut parts = Vec::new();

            // If the character is a digit, let's process it
            if c.is_ascii_digit() {
                let mut part_number = 0;

                loop {
                    part_number *= 10;
                    part_number += c.to_digit(10).unwrap();

                    // Check if the digit is surrounded by symbols (not including
                    // digits and periods).  We check orthogonally and diagonally.
                    let mut new_parts = self.schematic.get_parts(self.x, self.y);
                    parts.append(&mut new_parts);

                    // Move to the next character
                    if self.advance() {
                        // We've reached the end of the number because we've
                        // reached the end of the line.
                        return Some((part_number, dedup_parts(parts)));
                    }

                    // Update c for the next iteration
                    c = self.get_char(self.x, self.y).unwrap();

                    if !c.is_ascii_digit() {
                        // We've reached the end of the number because we've
                        // reached a non-digit character.
                        return Some((part_number, dedup_parts(parts)));
                    }
                }
            }

            self.advance();
        }
    }
}
