mod schematic;

use std::collections::HashMap;

use crate::schematic::Schematic;

fn main() {
    let schematic = Schematic::load("./data/input.txt");

    let value: u32 = schematic
        .iter()
        .filter(|(_, part_syms)| !part_syms.is_empty())
        .map(|(part, _)| part)
        .sum();

    dbg!(value);

    let schematic = Schematic::load("./data/input.txt");
    let mut parts_map = HashMap::new();

    schematic.iter().for_each(|(part, part_syms)| {
        part_syms.into_iter().for_each(|part_sym| {
            parts_map
                .entry(part_sym)
                .or_insert_with(Vec::new)
                .push(part);
        });
    });

    let v: u32 = parts_map
        .into_iter()
        .filter(|(part, part_nums)| part_nums.len() > 1 && part.0 == '*')
        .map(|(_, part_nums)| part_nums.iter().product::<u32>())
        .sum();

    dbg!(v);
}
