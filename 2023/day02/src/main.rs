use std::{fs::read_to_string, str::Lines};

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{char as ch, digit1},
    multi::{many1, separated_list1},
    sequence::tuple,
    IResult,
};

fn main() {
    println!("Day 2");

    let file = read_to_string("./data/input.txt").unwrap();
    let games = Games::new(file.lines());

    let max_red = 12;
    let max_green = 13;
    let max_blue = 14;

    let total: usize = games
        .filter(|game| {
            game.rounds.iter().all(|round| {
                round.red <= max_red && round.green <= max_green && round.blue <= max_blue
            })
        })
        .map(|game| game.id)
        .sum();

    dbg!(total);

    let file = read_to_string("./data/input.txt").unwrap();
    let games = Games::new(file.lines());

    let power: usize = games
        .map(|game| {
            let max_cubes = game
                .rounds
                .iter()
                .fold((0, 0, 0), |(red, green, blue), round| {
                    (
                        red.max(round.red),
                        green.max(round.green),
                        blue.max(round.blue),
                    )
                });

            max_cubes.0 * max_cubes.1 * max_cubes.2
        })
        .sum();

    dbg!(power);
}

//
// Game information
//

// Represents the information for a single game.
#[derive(Debug)]
struct GameInfo {
    id: usize,
    rounds: Vec<Round>,
}

// Represents the information for a single round where the number of red, green
// and blue cubes are counted.
#[derive(Debug)]
struct Round {
    red: usize,
    green: usize,
    blue: usize,
}

// Represents the colour of a cube.
#[derive(Debug)]
enum Colour {
    Red,
    Green,
    Blue,
}

// Represents the number of cubes of a given colour.
#[derive(Debug)]
struct ColourCount {
    colour: Colour,
    count: usize,
}

impl GameInfo {
    fn parse(input: &str) -> IResult<&str, Self> {
        let (input, (_, game_id, _)) = tuple((tag("Game "), digit1, tag(": ")))(input)?;
        let (input, rounds) = separated_list1(tag("; "), Round::parse)(input)?;

        Ok((
            input,
            Self {
                id: game_id.parse().unwrap(),
                rounds,
            },
        ))
    }
}

impl Round {
    fn parse(input: &str) -> IResult<&str, Self> {
        // Parse the sequence <number> <colour>
        let (input, colours) = separated_list1(tag(", "), ColourCount::parse)(input)?;
        let mut red = 0;
        let mut green = 0;
        let mut blue = 0;
        for colour in colours {
            match colour.colour {
                Colour::Red => red += colour.count,
                Colour::Green => green += colour.count,
                Colour::Blue => blue += colour.count,
            }
        }

        Ok((input, Self { red, green, blue }))
    }
}

impl Colour {
    // Parse the words "red", "green" or "blue" and return the corresponding enum value
    fn parse(input: &str) -> IResult<&str, Self> {
        let (input, colour) = alt((tag("red"), tag("green"), tag("blue")))(input)?;
        match colour {
            "red" => Ok((input, Colour::Red)),
            "green" => Ok((input, Colour::Green)),
            "blue" => Ok((input, Colour::Blue)),
            _ => unreachable!("Impossible colour"),
        }
    }
}

impl ColourCount {
    fn parse(input: &str) -> IResult<&str, Self> {
        // Parse the sequence <number> <colour>
        let (input, (count, _, colour)) = tuple((digit1, many1(ch(' ')), Colour::parse))(input)?;

        Ok((
            input,
            Self {
                colour,
                count: count.parse().unwrap(),
            },
        ))
    }
}

//
// Games state
//

struct Games<'lines> {
    lines: Lines<'lines>,
}

impl<'lines> Games<'lines> {
    fn new(lines: Lines<'lines>) -> Self {
        Self { lines }
    }
}

impl Iterator for Games<'_> {
    type Item = GameInfo;

    fn next(&mut self) -> Option<Self::Item> {
        let line = self.lines.next()?;
        let (_, game_info) = GameInfo::parse(line).unwrap();
        Some(game_info)
    }
}
